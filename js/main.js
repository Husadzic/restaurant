(function () {
    //Mobile menu 
    var hambButton, menuList;
    var menuIsOpen = false;
    hambButton = document.getElementById('hamburger-button');
    menuList = document.getElementById('collapse');

    var mobileMenu =  function(){
        hambButton.classList.toggle("rotate");
        if( hambButton.classList.contains("rotate")){
            menuList.classList.add('open');
            document.getElementsByTagName('body')[0].style.overflow = "hidden";
            menuIsOpen = true;
        }else{
            menuList.classList.remove('open');
            document.getElementsByTagName('body')[0].style.overflow = "initial";
            menuIsOpen = false;
        }

    };

    hambButton.onclick = function(evt){
        evt.preventDefault();
        mobileMenu();
    };
  
    menuList.onmouseup = function(evt){
        evt.preventDefault();
        if (menuIsOpen){
            mobileMenu();
        }
    };
        
    //Image gallery lightbox init
    var galleryWrapper = document.getElementById('gallery-wrapper');
    var galleryItemsPath = [
        'images/gallery/caroline-attwood-576169-unsplash.jpg',
        'images/gallery/photo-1445979323117-80453f573b71.jpg',
        'images/gallery/photo-1482049016688-2d3e1b311543.jpg',
        'images/gallery/photo-1503788105720-433331157fad.jpg',
        'images/gallery/photo-1511910849309-0dffb8785146.jpg',
    ]
    for (item in galleryItemsPath){
        var lightbox = document.createElement('a');
        var picture = document.createElement('div');
            picture.classList.add('gallery-item');
            lightbox.setAttribute("href",galleryItemsPath[item]);
            lightbox.setAttribute("data-lightbox","gallery");
        picture.style.backgroundImage = "url('"+galleryItemsPath[item]+"')";
        lightbox.append(picture)
        galleryWrapper.append(lightbox);
    };

    var backButtons = document.getElementsByClassName('back-to-top');
    for (button in backButtons){
        backButtons[button].onclick = function(evt){
            evt.preventDefault();
            window.scrollTo(0,0);
        };
    };
    //Modal
    var modalTrgger = document.getElementById('modal-trigger');
    var modalWrapper = document.getElementsByClassName('modal-wrapper');
    var modalClose = document.getElementsByClassName('close');
    modalTrgger.onclick = function(evt){
        evt.preventDefault();
        modalWrapper[0].style.display = "block";
    };
    modalClose[0].onclick = function(evt){
        evt.preventDefault();
        modalWrapper[0].style.display = "none";
    }
    //Like
    var likes = document.getElementsByClassName('like-icon');
        for (like in likes){
            likes[like].onclick = function(evt){
                evt.preventDefault();
                var numLike = evt.target.firstChild.textContent;
                var number = parseInt(numLike,10);
                if(evt.target.classList.contains('far')){
                    evt.target.classList.remove('far');
                    evt.target.classList.add('fas');
                    number +=1;
                    evt.target.firstChild.innerHTML = number;
                }else {
                    evt.target.classList.add('far');
                    evt.target.classList.remove('fas');
                    number -=1;
                    evt.target.firstChild.innerHTML = number;
                }
                
            };
        }

})();


/*Hero slider start*/ 
$(document).ready(function(){
    $('.slider').slick({
        dots: true,
        arrows:true,
        responsive:[
            {
                breakpoint:990,
                    settings:{
                        dots:false,
                        arrows: false,
                        cssEase: 'linear',
                        adaptiveHeight: true
                    },
                breakpoint:1024,
                    settings:{
                        dots:false,
                        arrows:false,
                        cssEase: 'linear'
                    }
            }
        ]
    });
  });
/*Hero slider  end*/ 
/*Dishes slider start*/ 
$(document).ready(function(){
    $('.dishes-slider').slick({
        dots: true,
        infinite: true,
        arrows:false,
        speed: 400,
        slidesToShow: 4,
        slidesToScroll: 4,
        autoplay: true,
        autoplaySpeed: 3000,
        responsive: [
            {
            breakpoint: 1024,
            settings: {
                slidesToShow: 3,
                slidesToScroll: 3,
                infinite: true,
                dots: true
            }
            },
            {
            breakpoint: 600,
            settings: {
                slidesToShow: 2,
                slidesToScroll: 2
            }
            },
            {
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                slidesToScroll: 1
            }
            }
        ]
    });
  });
/*Dishes slider  end*/ 

